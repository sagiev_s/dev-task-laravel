<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StateTest extends TestCase
{
   use RefreshDatabase;

   public function setUp() :void {
   		parent::setUp();
   		$this->state = factory('App\State')->create();
   }

    /** @test */
    public function states_are_divided_into_counties()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->state->counties);
    }

    /** @test */
    public function output_the_overall_amount_of_taxes_collected_per_state()
    {
        $state = factory('App\State')->create();
        $countyOfState1 = factory('App\County')->create(['state_id' => $state->id, 'tax_amount' => 300]);
        $countyOfState2 = factory('App\County')->create(['state_id' => $state->id, 'tax_amount' => 200]);
        $countyNotOfState = factory('App\County')->create(['tax_amount' => 500]);

        $expectedOverallAmount = 500;

        $this->assertEquals($expectedOverallAmount, $state->overallAmount());
    }

    /** @test */
    public function output_the_average_amount_of_taxes_collected_per_state()
    {
        $state = factory('App\State')->create();
        $countyOfState1 = factory('App\County')->create(['state_id' => $state->id, 'tax_amount' => 300]);
        $countyOfState2 = factory('App\County')->create(['state_id' => $state->id, 'tax_amount' => 200]);
        $countyNotOfState = factory('App\County')->create(['tax_amount' => 500]);

        $expectedAverageAmount = 250;

        $this->assertEquals($expectedAverageAmount, $state->averageAmount());
    }

    /** @test */
    public function output_the_average_county_tax_rate_per_state()
    {
        $state = factory('App\State')->create();
        $countyOfState1 = factory('App\County')->create(['state_id' => $state->id, 'tax_rate' => 4]);
        $countyOfState2 = factory('App\County')->create(['state_id' => $state->id, 'tax_rate' => 2]);
        $countyNotOfState = factory('App\County')->create(['tax_rate' => 7]);

        $expectedAverageRate = 3;

        $this->assertEquals($expectedAverageRate, $state->averageRate());
    }
}
