<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountryTest extends TestCase
{
   use RefreshDatabase;
    /** @test */
    public function a_country_is_organized_in_states()
    {
        $country = factory('App\Country')->create();
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $country->states);
    }

    /** @test */
    public function output_the_average_tax_rate_of_the_country()
    {
        $country = factory('App\Country')->create();

        $stateOfCountry1 = factory('App\State')->create(['country_id' => $country]);
        $firstCountyOfFirstState = factory('App\County')->create(['state_id' => $stateOfCountry1->id, 'tax_rate' => 5]);
        $secondCountyOfFirstState = factory('App\County')->create(['state_id' => $stateOfCountry1->id, 'tax_rate' => 2]);
        $thirdCountyOfFirstState = factory('App\County')->create(['state_id' => $stateOfCountry1->id, 'tax_rate' => 11]);

        $stateOfCountry2 = factory('App\State')->create(['country_id' => $country]);
        $firstCountyOfSecondState = factory('App\County')->create(['state_id' => $stateOfCountry2->id, 'tax_rate' => 2]);
        $secondCountyOfSecondState = factory('App\County')->create(['state_id' => $stateOfCountry2->id, 'tax_rate' => 6]);

        $countyNotOfAnyState = factory('App\County')->create(['tax_rate' => 3]);

        $expectedAverageRate = 5.2; // (5+2+11+2+6)/5

        $this->assertEquals($expectedAverageRate, $country->averageRate());
    }

    /** @test */
    public function output_the_overall_taxes_of_the_country()
    {
        $country = factory('App\Country')->create();

        $stateOfCountry1 = factory('App\State')->create(['country_id' => $country]);
        $firstCountyOfFirstState = factory('App\County')->create(['state_id' => $stateOfCountry1->id, 'tax_amount' => 1000]);
        $secondCountyOfFirstState = factory('App\County')->create(['state_id' => $stateOfCountry1->id, 'tax_amount' => 4000]);
        $thirdCountyOfFirstState = factory('App\County')->create(['state_id' => $stateOfCountry1->id, 'tax_amount' => 3000]);

        $stateOfCountry2 = factory('App\State')->create(['country_id' => $country]);
        $firstCountyOfSecondState = factory('App\County')->create(['state_id' => $stateOfCountry2->id, 'tax_amount' => 2000]);
        $secondCountyOfSecondState = factory('App\County')->create(['state_id' => $stateOfCountry2->id, 'tax_amount' => 8000]);

        $countyNotOfAnyState = factory('App\County')->create(['tax_amount' => 3000]);

        $expectedOverallAmount = 18000; // 1000+4000+3000+2000+8000 = 18000

        $this->assertEquals($expectedOverallAmount, $country->overallAmount());
    }
}
