<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public function counties() {
    	return $this->hasMany(County::class);
    }

    public function overallAmount() {
    	return $this->counties()
    		->selectRaw('ifnull(sum(tax_amount), 0) as overall_amount')
    		->first()
    		->overall_amount;
    }

    public function averageAmount() {
    	return $this->counties()
    		->selectRaw('ifnull(avg(tax_amount), 0) as average_amount')
    		->first()
    		->average_amount;
    }

    public function averageRate() {
    	return $this->counties()
    		->selectRaw('ifnull(avg(tax_rate), 0) as average_rate')
    		->first()
    		->average_rate;
    }
}
