<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Country extends Model
{
    public function states() {
    	return $this->hasMany(State::class);
    }

    public function averageRate() {
    	return DB::table('countries')
    		->join('states', 'countries.id', '=', 'states.country_id')
    		->join('counties', 'states.id', '=', 'counties.state_id')
    		->where('countries.id', $this->id)
    		->selectRaw('avg(tax_rate) average_rate')
    		->first()
            ->average_rate;
    }

    public function overallAmount() {
        return DB::table('countries')
            ->join('states', 'countries.id', '=', 'states.country_id')
            ->join('counties', 'states.id', '=', 'counties.state_id')
            ->where('countries.id', $this->id)
            ->selectRaw('sum(tax_amount) overall_amount')
            ->first()
            ->overall_amount;
    }
}
