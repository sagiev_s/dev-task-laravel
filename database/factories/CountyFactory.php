<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\County;
use Faker\Generator as Faker;

$factory->define(County::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'state_id' => factory('App\State')->create()->id,
        'tax_rate' => $faker->numberBetween(1,10),
        'tax_amount' => $faker->numberBetween(1,20) * 1000,
    ];
});
